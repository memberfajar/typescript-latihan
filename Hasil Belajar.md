# TypeScript
## Basic Type
Tipe data yang dimiliki TypeScript
* Boolean --> memiliki nilai (true, false)
* Number --> floating point values.
    let decimal: number = 6;
    let hex: number = 0xf00d;
    let binary: number = 0b1010;
    let octal: number = 0o744;
* String
    let color: string = "blue";
    color = 'red';
* Array
* Tupe
* Enum
* Any
* Void
* Null and Undefined
* Never
* Type assertions
* A note about let